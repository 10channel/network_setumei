/**
 * @file network.h
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief ネットワーク共通のヘッダ
 * @version 0.1
 * @date 2021-09-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _NETWORK_H_
#define _NETWORK_H_

#include <netinet/in.h>

#define DEFAULT_PORT 50100
#define MAX_LEN_NAMES 10
#define MAX_NUM_CLIENTS 5
#define MAX_LEN_BUFFER 256
#define MAX_LEN_ADDR 32
#define BROADCAST -1

typedef struct _CLIENT {
    int cid;
    int sock;
    struct sockaddr_in addr;
    char name[MAX_LEN_NAMES];
} CLINET;

typedef struct _CONTAINER {
    int cid;
    int command;
    int posx;
    int posy;
} CONTAINER;

typedef enum {
    NT_DEFAULT = 0,
    NT_END     = 1,
} NET_COMMAND;

#endif /*_NETWORK_H_*/
