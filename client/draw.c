/**
 * @file draw.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief 描画関連の関数
 * @version 0.1
 * @date 2021-09-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "client.h"
#include "header.h"
/**
 * @brief Set the endian object
 * 
 * @param little 
 * @return Uint32 エンディアン変換後の値
 */
Uint32 set_endian(Uint32 little)
{
    //リトルエンディアンを基準
    int x = 1; //0x00000001
    Uint32 bit;

    if (*(char*)&x) {
        // little endian. 01 00 00 00
        bit = little;
    } else {
        // big endian 00 00 00 01
        bit = SDL_Swap32(little);
    }

    return bit;
}

/**
 * @brief 描画処理
 * 
 * @param window ウィンドウ（ポインター）
 * @param renderer レンダラー（ポインター）
 * @return int 0
 */
int draw_drawer(SDL_Window* window, SDL_Renderer* renderer)
{
    SDL_Point befPoint[MAX_NUM_CLIENTS];
    Uint32 red     = set_endian(0xff0000ff);
    Uint32 blue    = set_endian(0xffff0000);
    Uint32 white   = set_endian(0xffffffff);
    int client_num = return_client_num();
    int myid       = return_myid(); //自身のクライアントID
    SDL_Point draw_point;
    //SDLの描画処理
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    //円を描画
    retun_client_point(myid, &(draw_point.x), &(draw_point.y));
    filledCircleColor(renderer, draw_point.x, draw_point.y, 10, red);
    befPoint[myid].x = draw_point.x;
    befPoint[myid].y = draw_point.y;
    //描画内容を反映
    SDL_RenderPresent(renderer);
    while (director.status == GS_PLAYING) {
        if (director.draw_status == DR_DRAW) {
            int i;
            for (i = 0; i < client_num; i++) {
                retun_client_point(i, &(draw_point.x), &(draw_point.y));
                filledCircleColor(renderer, befPoint[i].x, befPoint[i].y, 10, white);
                if (myid != i) {
                    filledCircleColor(renderer, draw_point.x, draw_point.y, 10, red);
                } else {
                    filledCircleColor(renderer, draw_point.x, draw_point.y, 10, blue);
                }
                befPoint[i].x = draw_point.x;
                befPoint[i].y = draw_point.y;
            }

            SDL_RenderPresent(renderer);
            director.draw_status = DR_NONE;
        }
    }
    return 0;
}
