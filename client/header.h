#ifndef HEADER_H
#define HEADER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <time.h>

#define WINDOW_SIZE_X 1080
#define WINDOW_SIZE_Y 760

typedef struct _GameDirector {
    int status;
    int draw_status;
    SDL_Event event;
} GameDirector;

typedef struct chara {
    SDL_Point pos;
} CHARA;

typedef enum {
    GS_START   = 1,
    GS_PLAYING = 2,
    GS_END     = 3,
} GS_STTS;

typedef enum {
    DR_NONE = 0,
    DR_DRAW = 1,
} DR_STTS;

typedef enum {
    UP    = 1,
    RIGHT = 2,
    LEFT  = 3,
    DOWN  = 4,
} DIRECTION;

typedef enum {
    NOMAL = 5,
    RUN   = 10,
} SPEED;

extern GameDirector director;
extern CHARA chara;

extern int draw_setup(SDL_Window* window, SDL_Renderer* renderer);
extern int draw_drawer(SDL_Window* window, SDL_Renderer* renderer);
extern int func_keyboard(void* args);
extern int chara_move(const int dirction, const int speed);
#endif /* HEADER_H */