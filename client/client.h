/**
 * @file client.h
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief クライアント側のヘッダファイル
 * @version 0.1
 * @date 2021-09-08
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "../network.h"
#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

int send_pos(int x, int y);
int setup_network(int argc, char *argv[]);
int control_requests();
int return_client_num(); //クライアントの総数を返す

int retun_client_point(int cid, int *x, int *y); //クライアントごとの位置を返す
int return_myid();

#endif /* _CLIENT_H_ */