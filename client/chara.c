/**
 * @file chara.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief キャラクター動作関連
 * @version 0.1
 * @date 2021-09-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "client.h"
#include "header.h"
CHARA chara;

int chara_setpos(const int x, const int y)
{
    chara.pos.x = x;
    chara.pos.y = y;

    return 0;
}

int chara_move(const int dirction, const int speed)
{
    switch (dirction) {
    case UP:
        chara.pos.y -= speed;
        break;

    case DOWN:
        chara.pos.y += speed;
        break;

    case RIGHT:
        chara.pos.x += speed;
        break;

    case LEFT:
        chara.pos.x -= speed;
        break;
    }
    send_pos(chara.pos.x, chara.pos.y);

    return 0;
}