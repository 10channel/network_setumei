/**
 * @file input.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief 入力関連
 * @version 0.1
 * @date 2021-09-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "header.h"

int func_keyboard(void *args)
{
    while (director.status == GS_PLAYING) {
        if (SDL_PollEvent(&(director.event))) {
            switch (director.event.type) {
            case SDL_KEYDOWN:
                switch (director.event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    director.status = GS_END;
                    break;

                case SDLK_w:
                    chara_move(UP, NOMAL);
                    director.draw_status = DR_DRAW;
                    break;

                case SDLK_a:
                    chara_move(LEFT, NOMAL);
                    director.draw_status = DR_DRAW;
                    break;

                case SDLK_d:
                    chara_move(RIGHT, NOMAL);
                    director.draw_status = DR_DRAW;
                    break;

                case SDLK_s:
                    chara_move(DOWN, NOMAL);
                    director.draw_status = DR_DRAW;
                    break;
                }
                break;

            case SDL_QUIT:
                director.status = GS_END;
                break;
            }
        }
    }
    return 0;
}