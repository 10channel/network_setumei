/**
 * @file network.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief クライアント側のネットワーク処理
 * @version 0.1
 * @date 2021-09-08
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "client.h"
#include "header.h"

static int sock;        //ソケット番号
static int num_clients; //クライアント総数
static int myid;        //自身のクライアントID
static CLINET clients[MAX_NUM_CLIENTS];
static int num_sock; //作ったソケットの個数
static fd_set mask;  //ファイルディスクリプタを管理
static CONTAINER clients_data[MAX_NUM_CLIENTS];

static int setup_client(char *server_name, u_short port);
static int receve();
static void handle_error(char *message);
static void send_data(void *data, int size);
static int receive_data(void *data, int size);
static void terminate_client();

int setup_network(int argc, char *argv[])
{
    u_short port = DEFAULT_PORT;
    char server_name[MAX_LEN_NAMES];

    sprintf(server_name, "localhost");

    switch (argc) {
    case 1:
        break;
    case 3:
        port = (u_short)atoi(argv[2]);
    case 2:
        sprintf(server_name, "%s", argv[1]);
        break;

    default:
        fprintf(stderr, "Usage:%s [server_name] [port_number]\n", argv[0]);
        return 1;
    }
    setup_client(server_name, port);
}

int setup_client(char *server_name, u_short port)
{
    struct hostent *server;     //serverのIPアドレスを格納
    struct sockaddr_in sv_addr; //サーバーの設定情報を格納

    fprintf(stderr, "Trying to connect server %s (port = %d).\n", server_name, port);
    if ((server = gethostbyname(server_name)) == NULL) { //ホスト名からIPアドレスを取得
        handle_error("gethostbyname()");                 //エラー表示
    }

    sock = socket(AF_INET, SOCK_STREAM, 0); //ソケットの作成
    if (sock < 0) {
        handle_error("socket()");
    }
    //サーバーの設定情報を格納
    sv_addr.sin_family      = AF_INET; //インターネットを使用
    sv_addr.sin_port        = htons(port);
    sv_addr.sin_addr.s_addr = *(u_int *)server->h_addr_list[0]; //ネームサーバーから得たサーバーIPアドレスを設定

    if (connect(sock, (struct sockaddr *)&sv_addr, sizeof(sv_addr)) != 0) { //作成したソケットからサーバーに対して接続要求
        handle_error("connect()");
    }

    fprintf(stderr, "Input your name:");
    char user_name[MAX_LEN_NAMES]; //入力用の変数を作成
    if (fgets(user_name, sizeof(user_name), stdin) == NULL) {
        handle_error("fgets()");
    }

    user_name[strlen(user_name) - 1] = '\0'; //最後に終了文字を入れる
    send_data(user_name, MAX_LEN_NAMES);

    fprintf(stderr, "Waiting for other clients...\n");
    receive_data(&num_clients, sizeof(int));
    fprintf(stderr, "Number of clients = %d.\n", num_clients);
    receive_data(&myid, sizeof(int));
    fprintf(stderr, "Your ID = %d.\n", myid);
    int i;
    for (i = 0; i < num_clients; i++) {
        receive_data(&clients, sizeof(CLINET)); //ホストから送られてきた、すべてのクライアント情報を格納
    }

    num_sock = sock + 1;
    FD_ZERO(&mask);      //maskを初期化
    FD_SET(0, &mask);    //標準入力をmaskに追加
    FD_SET(sock, &mask); //ソケットをmaskに追加
}

int control_requests()
{
    int cond = 1;
    while (cond) {
        fd_set read_flag = mask; //read_flagに登録されているファイルディスクリプタ(標準出力とserverのソケット)をコピー

        struct timeval timeout; //タイムアウト時間を設定
        timeout.tv_sec  = 0;    //秒
        timeout.tv_usec = 30;   //ミリ秒
        //0.03秒をタイムアウト時間として設定

        if (select(num_sock, (fd_set *)&read_flag, NULL, NULL, &timeout) == -1) {
            handle_error("select();");
        }

        int result = 1;
        if (FD_ISSET(sock, &read_flag)) {
            cond                 = receve();
            director.draw_status = DR_DRAW;
        }
        if (director.status == GS_END) {
            cond = 0;
        }
    }

    terminate_client();
}

int end_network()
{
}

int return_client_num()
{
    return num_clients;
}

int retun_client_point(int cid, int *x, int *y)
{
    SDL_Point point;
    *x = clients_data[cid].posx;
    *y = clients_data[cid].posy;
    return 0;
}

int return_myid()
{
    return myid;
}

//ポジションを送信
int send_pos(int x, int y)
{
    CONTAINER data;
    int result = 1;
    memset(&data, 0, sizeof(CONTAINER));
    clients_data[myid].cid     = myid;
    clients_data[myid].command = NT_DEFAULT;
    clients_data[myid].posx    = x;
    clients_data[myid].posy    = y;
    data                       = clients_data[myid];
    send_data(&data, sizeof(data));
}

static int receve()
{
    CONTAINER data;
    int result = 1;
    memset(&data, 0, sizeof(CONTAINER));
    receive_data(&data, sizeof(data));
    clients_data[data.cid] = data;
    if (data.command == NT_END) {
        result = 0;
    }
    return result;
}

static void handle_error(char *message)
{
    perror(message);
    fprintf(stderr, "%d\n", errno);
    exit(1);
}
static void send_data(void *data, int size)
{
    if ((data == NULL) || (size <= 0)) {
        fprintf(stderr, "send_data(): data is illeagal.\n");
        exit(1);
    }
    if (write(sock, data, size) == -1) {
        handle_error("write()");
    }
}

static int receive_data(void *data, int size)
{
    if ((data == NULL) || (size <= 0)) {
        fprintf(stderr, "receive_data(): data is illeagal.\n");
        exit(1);
    }
    return (read(sock, data, size));
}
static void terminate_client()
{
    fprintf(stderr, "Connection is closed.\n");
    close(sock);
    exit(0);
}