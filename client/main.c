/**
 * @file main.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief メイン関数など
 * @version 0.1
 * @date 2021-09-06
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "client.h"
#include "header.h"

//構造体
GameDirector director;

/**
 * @brief メイン関数
 * 
 * @param argc 引数の個数
 * @param argv 引数の値
 * @return int 0
 */
int main(int argc, char* argv[])
{
    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Thread* keyboard_thread;
    SDL_Thread* network_thread;

    //変数値の初期化
    director.status = GS_START;
    srand((unsigned int)time(NULL));
    //プレイヤー位置の初期化
    chara.pos.x = rand() % WINDOW_SIZE_X;
    chara.pos.y = rand() % WINDOW_SIZE_Y;

    //SDL初期化
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
        printf("failed to setup SDL2.\n");
        exit(-1);
    }
    //ウィンドウ生成
    if ((window = SDL_CreateWindow("Network_test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_SIZE_X, WINDOW_SIZE_Y, 0)) == NULL) {
        printf("failed to create window\n");
        exit(-1);
    }
    //レンダリングコンテキスト生成
    if ((renderer = SDL_CreateRenderer(window, -1, 0)) == NULL) {
        printf("failed to create renderer.\n");
        exit(-1);
    }
    setup_network(argc, argv);
    director.status = GS_PLAYING;

    //draw_setup(window, renderer);

    //スレッド起動
    keyboard_thread = SDL_CreateThread(func_keyboard, "keyboard theard", NULL);
    network_thread  = SDL_CreateThread(control_requests, "network_thread", NULL);
    send_pos(chara.pos.x, chara.pos.y);
    draw_drawer(window, renderer);

    SDL_WaitThread(keyboard_thread, NULL);
    SDL_WaitThread(network_thread, NULL);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    printf("finished\n");
    return 0;
}
