/**
 * @file server.h
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief サーバープログラム
 * @version 0.1
 * @date 2021-09-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _SERVER_H_
#define _SERVER_H_
#include "../network.h"
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

extern void setup_server(int num_cl, u_short port);
extern int control_requests();
extern void terminate_server(void);
#endif /*_SERVER_H_*/