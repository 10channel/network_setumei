/**
 * @file server.c
 * @author Shoma Okui (shoma20012001@gmail.com)
 * @brief 通信関連（サーバー側）
 * @version 0.1
 * @date 2021-09-07
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "server.h"

int main(int argc, char *argv[])
{
    int num_cl   = 1;            //接続数
    u_short port = DEFAULT_PORT; //ポート番号

    //コマンドの個数により条件武器
    switch (argc) {
    case 1:
        break;

    case 3:
        port = atoi(argv[2]); //ポート番号を整数値に変換して代入
    case 2:
        num_cl = atoi(argv[1]); //接続数を整数値にに変換して代入
        break;

    default:
        fprintf(stderr, "Usage: %s [number of clients] [port number]\n", argv[0]);
        return 1;
    }
    //エラー判定
    if (num_cl < 0 || num_cl > MAX_NUM_CLIENTS) {
        fprintf(stderr, "Max number of clients is %d\n", MAX_NUM_CLIENTS);
        return 1;
    }

    fprintf(stderr, "Number of clients = %d\n", num_cl);
    fprintf(stderr, "Port number = %d\n", port);

    setup_server(num_cl, port);

    int cond = 1;
    while (cond) {
        cond = control_requests();
    }

    terminate_server();

    return 0;
}