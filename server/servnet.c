#include "server.h"

static int num_clients;
static int num_socks;

static void handle_error(char* message);
static CLINET clients[MAX_NUM_CLIENTS];
static fd_set mask;    //ファイルディスクリプタを登録する
static CONTAINER data; //送受信に利用するコンテナ
static CONTAINER clients_data[MAX_NUM_CLIENTS];

int control_requests();
static void send_data(int cid, void* data, int size);
static int receive_data(int cid, void* data, int size);
static void handle_error(char* message);
void terminate_server(void);

void setup_server(int num_cl, u_short port)
{
    int rsock, sock = 0;                 //ソケット番号
    struct sockaddr_in sv_addr, cl_addr; //sockaddr_inを格納

    //サーバーのセットアップを開始することを表示
    fprintf(stderr, "Server setup is started\n");

    //h奇数で渡された接続数をnum_clientsに代入
    num_clients = num_cl;
    rsock       = socket(AF_INET, SOCK_STREAM, 0); //ソケットの生成
    if (rsock < 0) {
        handle_error("socket()");
    }

    fprintf(stderr, "sock () for request socket is done successfully.\n");

    //sv_addrに設定情報を代入
    sv_addr.sin_family      = AF_INET;     //インターネットを使用
    sv_addr.sin_port        = htons(port); //ポート番号を設定
    sv_addr.sin_addr.s_addr = INADDR_ANY;  //すべてのアドレスから入力を受け付ける

    int opt = 1;
    //ソケットのオプション設定
    setsockopt(rsock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    //rsockとsvaddrを関連付けるサーバー設定情報とソケットを接続
    if (bind(rsock, (struct sockaddr*)&sv_addr, sizeof(sv_addr)) != 0) {
        handle_error("bind()");
    }
    fprintf(stderr, "bind () is done successfully.\n");
    if (listen(rsock, num_clients) != 0) {

        handle_error("listen()");
    }
    fprintf(stderr, "listen() is started.\n");

    int i, max_sock;
    socklen_t len;
    char src[MAX_LEN_ADDR];
    for (i = 0; i < num_clients; i++) {
        len  = sizeof(cl_addr);
        sock = accept(rsock, (struct sockaddr*)&cl_addr, &len); //クライアントからの接続リクエストを受け取る
        if (sock < 0) {
            handle_error("accept()"); //エラーを返す
        }
        if (max_sock < sock) {
            max_sock = sock; //ソケットで一番最後に接続されたものをmax_sockに代入(select用)
        }
        if (read(sock, clients[i].name, MAX_LEN_NAMES) == -1) { //クライアントから送られてきたクライアント名を読み込むみ代入
            handle_error("read()");
        }
        clients[i].cid  = i;         //クライアントID
        clients[i].sock = sock;      //ソケットを設定　同一
        clients[i].addr = cl_addr;   //クライアント用のcl_addrを格納
        memset(src, 0, sizeof(src)); //srcを0にリセット
        inet_ntop(AF_INET, (struct sockaddr*)&cl_addr.sin_addr, src, sizeof(src));
        fprintf(stderr, "client %d is accepted (name=%s, address=%s, port=%d", i, clients[i].name, src, ntohs(cl_addr.sin_port)); //接続したクライアント情報を表示
    }

    close(rsock); //rsockを破棄する
    int j;
    for (i = 0; i < num_clients; i++) {
        send_data(i, &num_clients, sizeof(int));
        send_data(i, &i, sizeof(int)); //クライアントに接続した順番cidを送信
        for (j = 0; j < num_clients; j++) {
            send_data(i, &clients[j], sizeof(CLINET));
        }
    }

    num_socks = max_sock + 1; //select用

    FD_ZERO(&mask);   //maskをクリア
    FD_SET(0, &mask); //0番標準入力のファイルディスクリプタをmaskに登録

    for (i = 0; i < num_clients; i++) {
        FD_SET(clients[i].sock, &mask); //ソケットのファイルディスクリプタをmaskにセット
    }
    fprintf(stderr, "Server setup is done.\n"); //サーバーセットアップの終了通知
}

int control_requests()
{
    fd_set read_flag = mask;             //登録しているファイルディスクリプタの情報をread_flagにコピー
    memset(&data, 0, sizeof(CONTAINER)); //dataを初期化(すべて0にする)

    fprintf(stderr, "select() is started.\n");

    //selectでread_flag(mask)に登録したファイルディスクリプタ（ここでは標準入力、ソケット）を監視する。
    if (select(num_socks, (fd_set*)&read_flag, NULL, NULL, NULL) == -1) {
        handle_error("select()");
    }

    int i, result = 1;
    for (i = 0; i < num_clients; i++) {
        //clientsのソケットが読み込み可能化を見る
        if (FD_ISSET(clients[i].sock, &read_flag)) {
            receive_data(i, &data, sizeof(data)); //データを受信する
            clients_data[i] = data;
        }
    }
    for (i = 0; i < num_clients; i++) { //最新のデータをすべてのクライアントに送信する
        int j;

        memset(&data, 0, sizeof(data));
        data = clients_data[i];
        send_data(BROADCAST, &data, sizeof(data));
        if (data.command == NT_END)
            return 0; //終了処理
    }

    return 1;
}

static void send_data(int cid, void* data, int size)
{
    if ((cid != BROADCAST) && (0 > cid || cid >= num_clients)) {
        fprintf(stderr, "send_date():client id is illeagal.\n");
        exit(1);
    }

    if ((data == NULL) || (size <= 0)) {
        fprintf(stderr, "send_data():data is illeagal.\n");
        exit(1);
    }

    if (cid == BROADCAST) {
        int i;
        for (i = 0; i < num_clients; i++) {
            if (write(clients[i].sock, data, size) < 0) {
                handle_error("write()");
            }
        }
    } else {
        if (write(clients[cid].sock, data, size) < 0) {
            handle_error("write()");
        }
    }
}

static int receive_data(int cid, void* data, int size)
{
    if ((cid != BROADCAST) && (0 > cid || cid >= num_clients)) {
        fprintf(stderr, "receive_data(): client id is illeagal.\n");
        exit(1);
    }
    if ((data == NULL) || size <= 0) {
        fprintf(stderr, "receive_data(): data is illeagal.\n");
        exit(1);
    }

    return read(clients[cid].sock, data, size);
}

static void handle_error(char* message)
{
    perror(message);
    fprintf(stderr, "%d\n", errno);
    exit(1);
}

void terminate_server(void)
{
    int i;
    for (i = 0; i < num_clients; i++) {
        close(clients[i].sock);
    }
    fprintf(stderr, "aALL connection are closed.\n");
    exit(0);
}